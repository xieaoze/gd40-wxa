// placeorder.js  下订单
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        date: '',
        products: [],
        isHaveAddress: false,
        isGetAddress: false,
        address: {},
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this;
        // 获取收货信息列表
        app.getDeliveryList(function(params) {
                if (params && params.length > 0) {
                    app.globalData.addresses = params;
                    that.setData({
                        isHaveAddress: true,
                    })
                }
            }, function(msg) {

            })
            // 获取添加产品回调参数
        if (options.isAddSuccess) {
            console.log(options)
            var product = {
                id: '',
                name: app.globalData.product_name,
                num: app.globalData.product_num,
                desc: ''
            };
            app.globalData.products.push(product);
            that.setData({
                products: app.globalData.products,
            })
        }
        console.log(app.globalData.addressIndex);
        // 获取选择的收货信息
        if (app.globalData.addressIndex != -1) {
            var index = app.globalData.addressIndex;
            console.log(app.globalData.addresses[index])
            that.setData({
                isGetAddress: true,
                address: app.globalData.addresses[index]
            })
        }
        if (app.globalData.deliveryDate) {
            that.setData({
                date: app.globalData.deliveryDate
            })
        }
    },
    /**
     * 添加日期
     */
    bindDateChange: function(e) {
        app.globalData.deliveryDate = e.detail.value;
        this.setData({
            date: e.detail.value
        })
    },

    inputMore: function() {
        wx.navigateTo({
            url: '/pages/inputinfo/inputinfo',
        })
    },

    addProduct: function() {
        wx.redirectTo({
            url: '/pages/placeorder/addproduct/addproduct',
        })
    },

    /**
     * 跳转到新增收货地址页面
     */
    goToAddDelivery: function() {
        wx.redirectTo({
            url: '/pages/placeorder/deliveryinfo/deliveryinfo',
        })
    },
    /**
     * 跳转到收货地址列表页面
     */
    goToChooseDelivery: function() {
        wx.redirectTo({
            url: '/pages/choosedelivery/choosedelivery',
        })
    },
    /**
     * 跳转到验证手机页面
     */
    goToTestPhone: function() {
        app.globalData.orderDetail = {
            code: '',
            products: app.globalData.products,
            attachment: [],
            desc: '',
            due: app.globalData.deliveryDate,
            delivery_id: this.data.address.id
        }
        wx.navigateTo({
            url: '/pages/testphone/testphone',
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {
        // app.globalData.products = [];
        // app.globalData.addressIndex = -1;
        // app.globalData.addresses = [];
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})