// orderbar.js

var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        todayTopic: 100,
        activeUser: 200,
        topicList: [{
            "id": "1",
            "title": "扫地方就爱上的纠纷",
            "view_n": 100,
            "reply_n": 200,
            "user": {
                // 用户基本信息
            },
            "time": "2017-10-10",
            "top": 1, // 是否置顶推荐
        }, {
            "id": "2",
            "title": "xxxaasdfasdfasdf",
            "view_n": 180,
            "reply_n": 200,
            "user": {
                // 用户基本信息
            },
            "time": "2017-10-10",
            "top": 1, // 是否置顶推荐
        }, {
            "id": "3",
            "title": "xxxdfasdfasdfsadf",
            "view_n": 150,
            "reply_n": 300,
            "user": {
                // 用户基本信息
            },
            "time": "2017-10-10",
            "top": 1, // 是否置顶推荐
        }, {
            "id": "3",
            "title": "xxxasdfasdfasdfasfd",
            "view_n": 150,
            "reply_n": 300,
            "user": {
                // 用户基本信息
            },
            "time": "2017-10-10",
            "top": 1, // 是否置顶推荐
        }, {
            "id": "3",
            "title": "xxxasdfasdfasdfsadf",
            "view_n": 150,
            "reply_n": 300,
            "user": {
                // 用户基本信息
            },
            "time": "2017-10-10",
            "top": 1, // 是否置顶推荐
        }, {
            "id": "3",
            "title": "xxx",
            "view_n": 150,
            "reply_n": 300,
            "user": {
                // 用户基本信息
            },
            "time": "2017-10-10",
            "top": 1, // 是否置顶推荐
        }, {
            "id": "3",
            "title": "xxx",
            "view_n": 150,
            "reply_n": 300,
            "user": {
                // 用户基本信息
            },
            "time": "2017-10-10",
            "top": 1, // 是否置顶推荐
        }]

    },

    placeOrder: function() {
        if (app.globalData.isLogin) {
            wx.redirectTo({
                url: '/pages/placeorder/useragreement/useragreement',
            })

        } else {
            wx.navigateTo({
                url: '/pages/login/login',
            })

        }
        // wx.showModal({
        //     title: '认证',
        //     content: '您的账号没有实名认证,订单创建后,需全额付款.',
        //     showCancel: true,
        //     cancelText: '继续下单',
        //     cancelColor: '#ccc',
        //     confirmText: '实名认证',
        //     confirmColor: '#4caf50',
        //     success: function(res) {
        //         console.log(res);
        //         if (res.confirm) {
        //         }
        //         if (res.cancel) {
        //         }
        //     },
        //     fail: function(res) {},
        //     complete: function(res) {},
        // })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        app.getBbsList(function(res) {
            // console.log(res)
        }, function() {

        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})