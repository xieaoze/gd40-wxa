// placesuccess.js
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        to_n: 0,
        products: [],
        orderDetail: 'xxx',
        address: {},
        date: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this;
        that.setData({
            products: app.globalData.orderDetail.products,
            // address: app.globalData.address,
            date: app.globalData.orderDetail.due
        })
        if (options.to_n) {
            that.setData({
                to_n: options.to_n
            })
        }
        if (app.globalData.addressIndex != -1) {
            var index = app.globalData.addressIndex;
            // console.log(app.globalData.addresses[index])
            that.setData({
                // isGetAddress: true,
                address: app.globalData.addresses[index]
            })
        }

    },

    bindDateChange: function(e) {
        this.setData({
            date: e.detail.value
        })
    },

    returnHome: function() {
        wx.reLaunch({
            url: '/pages/index/index',
        })
    },

    continueOrder: function() {
        wx.redirectTo({
            url: '/pages/placeorder/placeorder',
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {
        app.globalData.products = [];
        app.globalData.addressIndex = -1;
        app.globalData.addresses = [];
        app.globalData.orderDetail = {};
        app.globalData.product_name = '';
        app.globalData.product_num = 0;
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})