// deliveryinfo.js
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        // region: [],
        name: '',
        phone: '',
        city: '',
        address: '',
    },

    confirm: function() {
        var params = {
            mobile: this.data.phone,
            contact: this.data.name,
            address: this.data.address
        };
        app.addNewDelivery(params, function(res) {
            wx.redirectTo({
                url: '/pages/choosedelivery/choosedelivery?addSuccess=' + true,
            })
        }, function(msg) {
            app.warning(msg)
        })
    },

    // bindRegionChange: function(e) {
    //     console.log('picker发送选择改变，携带值为', e.detail.value)
    //     this.setData({
    //         region: e.detail.value
    //     })
    // },

    bindNameChange: function(e) {
        if (e.detail.value) {
            console.log(e.detail.value);
            this.setData({
                name: e.detail.value
            })
        }

    },
    bindPhoneChange: function(e) {
        if (e.detail.value) {
            console.log(e.detail.value);
            this.setData({
                phone: e.detail.value
            })
        }

    },
    bindCityChange: function(e) {
        if (e.detail.value) {
            console.log(e.detail.value);
            this.setData({
                city: e.detail.value
            })
        }

    },
    bindAddressChange: function(e) {
        if (e.detail.value) {
            console.log(e.detail.value);
            this.setData({
                address: e.detail.value
            })
        }

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})