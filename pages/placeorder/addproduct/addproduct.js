// addproduct.js
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        product_name: '',
        product_number: 0,
        orderDetail: '描述信息',
    },

    productName: function(e) {
        this.setData({
            product_name: e.detail.value
        })
    },
    productNum: function(e) {
        console.log(e);
        this.setData({
            product_number: Number(e.detail.value)
        })
    },

    inputMore: function() {
        wx.navigateTo({
            url: '/pages/inputinfo/inputinfo',
        })
    },

    addProduct: function() {
        app.globalData.product_name = this.data.product_name;
        app.globalData.product_num = this.data.product_number;
        console.log(app.globalData.product_name + '::' + app.globalData.product_name);
        wx.redirectTo({
            url: '/pages/placeorder/placeorder?isAddSuccess=' + true,
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})