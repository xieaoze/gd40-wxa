// useragreement.js --用户协议
var app = getApp();
var marked = require('../../../utils/marked.min.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        wemark: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this;
        marked.setOptions({
            renderer: new marked.Renderer(),
            gfm: true,
            tables: true,
            breaks: true,
            pedantic: false,
            sanitize: false,
            smartLists: true,
            smartypants: false
        });
        wx.request({
            url: app.globalData.url_test + 'v1/sys/license/demo',
            data: '',
            header: {},
            method: 'GET',
            dataType: '',
            success: function(res) {
                console.log(res.data)
                    // Using async version of marked
                marked(res.data, function(err, content) {
                    if (err) throw err;
                    console.log(content);
                    that.setData({
                        wemark: content
                    })
                });
                // var filePath = res.data
            },
            fail: function(res) {},
            complete: function(res) {},
        })
    },

    /**
     * 按钮点击事件
     */
    toAgree: function() {
        wx.redirectTo({
            url: '/pages/placeorder/placeorder',
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {},

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})