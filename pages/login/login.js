// login.js
var app = getApp()
var md5util = require('../../utils/md5.min.js')
Page({

    /**
     * 页面的初始数据
     */
    data: {
        phone: '',
        password: ''

    },

    phoneInput: function(params) {
        this.setData({
            phone: params.detail.value
        })
    },
    pwdInput: function(params) {
        this.setData({
            password: params.detail.value
        })
    },
    login: function() {
        var that = this;
        var reg = /^\s*|\s*$/g;
        var phone = that.data.phone;
        var password = that.data.password;
        console.log(md5util.md5(password));
        phone = phone.replace(reg, '');

        if (!phone && !password) {
            app.warning('手机号或密码不能为空');
            return;
        } else if (!password) {
            app.warning('请输入密码');
            return;
        } else if (!phone) {
            app.warning('请输入手机号');
            return;
        }
        var params = {
            mobile: phone,
            // mobile: '40099991003',
            pwd: md5util.md5(password),
            // code: jscode
        }
        app.loginIn(params,
            function(res) {
                if (res.id && res.id.length == 24) {
                    app.getToken(function(params) {
                        // app.globalData.token = res.token;
                        if (params.user.id) {
                            app.globalData.isLogin = true;
                        }
                    }, function(errMsg) {

                    })
                    wx.navigateBack({
                        // url: '/pages/index/index'
                    })
                } else {
                    console.log('id=' + res.id)
                }
            },
            function(errMsg) {
                app.warning(errMsg);
            })

        // wx.showToast({
        //   title: '登录中...',
        //   icon: 'loading',
        // });
        // wx.login({
        //   success: function (res) {
        //     var jscode = res.code;
        //     wx.request({
        //       url: 'http://121.201.13.231:8080' + '/v1/user/login',
        //       data: {
        //         mobile: phone,
        //         pwd: md5util.md5(password),
        //         // code: jscode
        //       },
        //       method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
        //       // header: {}, // 设置请求的 header
        //       success: function (res) {
        //         // success
        //         if (res.data.code == '0') {
        //           console.log(res.data.data);
        //           wx.navigateBack({
        //             url: '/pages/index/index'
        //           })
        //         } else {
        //           wx.hideToast();
        //           app.warning(res.data.msg);
        //         }
        //       },
        //       fail: function () {
        //         // fail
        //         wx.hideToast();
        //         app.warning('服务器无响应');
        //       },
        //       complete: function () {
        //         // complete
        //       }
        //     })
        //   },
        //   fail: function () {
        //     // fail
        //   },
        //   complete: function () {
        //     // complete
        //   }
        // })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})