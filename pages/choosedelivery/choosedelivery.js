// choosedelivery.js
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        deliveryList: [],
        // index: -1,
    },

    confirm: function() {
        if (app.globalData.addressIndex == -1) {
            app.warning('请选择一个收货地址')
        } else {
            wx.redirectTo({
                url: '/pages/placeorder/placeorder',
            })
        }
    },

    addAddress: function() {
        console.log(this.data.index);
        wx.redirectTo({
            url: '/pages/placeorder/deliveryinfo/deliveryinfo',
        })
    },

    radioChange: function(e) {
        console.log('radio发生change事件，携带value值为：', e.detail.value)
        app.globalData.addressIndex = e.detail.value
    },

    delDelivery: function(e) {
        var that = this;
        console.log(e.currentTarget.dataset.key)
        var params = {
            id: e.currentTarget.dataset.key
        }
        app.delDelivery(
                params,
                function(res) {
                    app.getDeliveryList(function(params) {
                        if (params && params.length > 0) {
                            app.globalData.addresses = params;
                            wx.redirectTo({
                                    url: '/pages/choosedelivery/choosedelivery',
                                })
                                // that.setData({
                                //     deliveryList: app.globalData.addresses
                                // })
                        }
                    }, function(msg) {

                    })
                },
                function(errMsg) {

                })
            // app.globalData.addressIndex = e.detail.value
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this;
        // console.log(app.globalData.addresses)
        if (options.addSuccess) {
            app.getDeliveryList(function(params) {
                if (params && params.length > 0) {
                    app.globalData.addresses = params;
                    that.setData({
                        deliveryList: app.globalData.addresses
                    })
                }
            }, function(msg) {

            })
        }
        that.setData({
            deliveryList: app.globalData.addresses
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})