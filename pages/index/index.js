var app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        menus: [{
            index: 1,
            title: '下订单',
            logo: '../../icon/order_a.png',
            color: '#ef5350',
            bindtap: 'placeOrder',
            detailNumb: '1450',
            detailInfo: '近30天订单总量',
        }, {
            index: 2,
            title: '接订单',
            logo: '../../icon/order_b.png',
            color: '#26a69a',
            bindtap: 'receiveOrder',
            detailNumb: '1450',
            detailInfo: '近30天订单总量',
        }, {
            index: 3,
            title: '我的订单',
            logo: '../../icon/order_c.png',
            color: '#42a5f5',
            bindtap: 'myOrders',
            detailNumb: '1450',
            detailInfo: '近30天订单总量',
        }, {
            index: 4,
            title: '企业信用',
            logo: '../../icon/credit.png',
            color: '#4caf50',
            bindtap: 'enterPriseCredit',
            detailNumb: '5.0',
            detailInfo: '信用度',
        }, {
            index: 5,
            title: '人才共享',
            logo: '../../icon/talents.png',
            color: '#f9a825',
            bindtap: 'personalSharing',
            detailNumb: '1450',
            detailInfo: '人才聚集总数',
        }, {
            index: 6,
            title: '我的福利',
            logo: '../../icon/welfare.png',
            color: '#ab47bc',
            bindtap: 'myWelfare',
            detailNumb: '1450',
            detailInfo: '获得福利汇总',
        }, {
            index: 7,
            title: '融资借贷',
            logo: '../../icon/financial.png',
            color: '#8d6e63',
            bindtap: 'financingOrLoan',
            detailNumb: '1450',
            detailInfo: '融资借贷总金额',
        }, {
            index: 8,
            title: '我要物流',
            logo: '../../icon/logistics.png',
            color: '#757575',
            bindtap: 'takeLogistics',
            detailNumb: '1450',
            detailInfo: '物流订单',
        }],
        // userInfo: {},
        // windowHeight: 0,
        cname: ''
    },

    bindViewTap: function() {},

    // register: function () {
    //   wx.navigateTo({
    //     url: '/pages/register/register',
    //   })
    // },
    showDetail: function() {
        // wx.navigateTo({
        //     url: '/pages/login/login',
        // })
    },
    /**
     * 下订单板块
     */
    placeOrder: function() {
        wx.navigateTo({
            url: '/pages/placeorder/orderbar/orderbar',
        })
    },
    /**
     * 接订单板块
     */
    receiveOrder: function() {

        if (app.globalData.isLogin) {
            wx.navigateTo({
                url: '/pages/receiveorder/receiveorder',
            })
        } else {
            wx.navigateTo({
                url: '/pages/login/login',
            })
        }
    },
    /**
     * 我的订单板块
     */
    myOrders: function() {
        wx.navigateTo({
            url: '/pages/myorders/myorders',
        })
    },
    /**
     * 企业信用板块
     */
    enterPriseCredit: function() {
        wx.navigateTo({
            url: '/pages/enterprisecredit/enterprisecredit',
        })
    },
    /**
     * 人才共享板块
     */
    personalSharing: function() {
        wx.navigateTo({
            url: '/pages/personalsharing/personalsharing',
        })
    },
    /**
     * 我的福利板块
     */
    myWelfare: function() {
        wx.navigateTo({
            url: '/pages/mywelfare/mywelfare',
        })
    },
    /**
     * 融资借贷板块
     */
    financingOrLoan: function() {
        wx.navigateTo({
            url: '/pages/financingorloan/financingorloan',
        })
    },
    /**
     * 我要物流板块
     */
    takeLogistics: function() {
        wx.navigateTo({
            url: '/pages/takelogistics/takelogistics',
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        console.log('isLogin:' + app.globalData.isLogin);
        var that = this
        var c_name = ''
            // console.log(options.cid)
        if (options.cid == 0) {
            c_name = '大辰金科技集团'
        } else if (options.cid == 1) {
            c_name = '深圳六加科技'
        } else {
            c_name = '企业名称'
        }
        that.setData({
                cname: c_name
            })
            // app.getUserInfo(function (res) {
            //   that.setData({
            //     userInfo: res
            //   })
            // })
            // wx.getSystemInfo({
            //   success: function (res) {
            //     console.log(res.model)
            //     that.setData({
            //       windowHeight: res.windowHeight
            //     })
            //   }
            // })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        app.getToken(function(params) {
            app.globalData.token = params.token;
            if (params.user.id) {
                app.globalData.isLogin = true;
            }
        }, function(errMsg) {

        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})