// companydirectory.js   "pages/companydirectory/companydirectory",
// var WxSearch = require('../../wxSearch/wxSearch.js');
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        indexImage: '/icon/index_bg.png',
        // grids: [0, 1, 2, 3, 4, 5, 6, 7, 8],
        // ../../../icon / avatar_default.jpg

    },



    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this;
        //初始化的时候渲染wxSearchdata
        // WxSearch.init(that, 43, ['变压器', '小程序', 'wxParse', 'wxSearch', 'wxNotification']);
        // 首次访问获取token
        app.getToken(
                function(res) {
                    app.globalData.token = res.token;
                },
                function() {

                })
            // WxSearch.initMindKeys(['weappdev.com', '微信小程序开发', '微信开发', '微信小程序']);
            // var URL = app.globalData.url_test + app.globalData.first_visit;
            // wx.request({
            //   url: URL,
            //   data: '',
            //   header: {},
            //   method: 'GET',
            //   dataType: '',
            //   success: function (res) {
            //     var data = res.data.data;
            //     app.globalData.token = data.token;
            //     wx.setStorageSync('TOKEN', data.token);
            //     console.log(data.token)
            //   },
            //   fail: function (res) {
            //     wx.showToast({
            //       title: '加载失败',
            //     })
            //   },
            //   complete: function (res) { },
            // })
    },

    wxSearchFn: function(e) {
        var that = this
            // WxSearch.wxSearchAddHisKey(that);
            // that.setData({
            //   isShow: true
            // })
        wx.redirectTo({
            url: '/pages/companysearch/searchresult/searchresult',
        })
    },
    // wxSearchInput: function (e) {
    //   var that = this
    //   WxSearch.wxSearchInput(e, that);
    // },
    // wxSerchFocus: function (e) {
    //   var that = this
    //   WxSearch.wxSearchFocus(e, that);
    // },
    // wxSearchBlur: function (e) {
    //   var that = this
    //   WxSearch.wxSearchBlur(e, that);
    // },
    // wxSearchKeyTap: function (e) {
    //   var that = this
    //   WxSearch.wxSearchKeyTap(e, that);
    // },
    // wxSearchDeleteKey: function (e) {
    //   var that = this
    //   WxSearch.wxSearchDeleteKey(e, that);
    // },
    // wxSearchDeleteAll: function (e) {
    //   var that = this;
    //   WxSearch.wxSearchDeleteAll(that);
    // },
    // wxSearchTap: function (e) {
    //   var that = this
    //   WxSearch.wxSearchHiddenPancel(that);
    // },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})