// searchresult.js
var WxSearch = require('../../../wxSearch/wxSearch.js');
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        isShow: false,
        companyNumber: 2,
        orderNumb: 2011,
        companyNumb: 100,
        money: 100000,
        companyList: [{
                id: 1,
                title: "全球变压器交易圈",
                manager: {},
                companyLogo: '/icon/avtar_a.png',
                companyName: '大辰金科技集团',
                stat: {
                    n1: 2011,
                    n2: 100,
                    n3: 2011
                }
            },
            {
                id: 2,
                title: "深圳变压器交易市场",
                manager: {},
                companyLogo: '/icon/avtar_b.png',
                companyName: '深圳六加科技',
                stat: {
                    n1: 2012,
                    n2: 100,
                    n3: 2013
                }
            }
        ]
    },
    go_in: function(e) {
        console.log(e.currentTarget)
        var cid = e.currentTarget.dataset.param
        wx.redirectTo({
            url: '/pages/index/index?cid=' + cid,
            success: function(res) {},
            fail: function(res) {},
            complete: function(res) {},
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this;
        //初始化的时候渲染wxSearchdata
        WxSearch.init(that, 88, ['变压器', '小程序', 'wxParse', 'wxSearch', 'wxNotification']);
    },
    wxSearchFn: function(e) {
        var that = this
        WxSearch.wxSearchAddHisKey(that);
        that.setData({
                isShow: true
            })
            // wx.redirectTo({
            //   url: '/pages/companysearch/searchresult/searchresult',
            // })
    },
    wxSearchInput: function(e) {
        var that = this
        WxSearch.wxSearchInput(e, that);
        console.log(e.currentTarget.dataset.key);
    },
    wxSerchFocus: function(e) {
        var that = this
        WxSearch.wxSearchFocus(e, that);
    },
    wxSearchBlur: function(e) {
        var that = this
        WxSearch.wxSearchBlur(e, that);
    },
    wxSearchKeyTap: function(e) {
        var that = this
        WxSearch.wxSearchKeyTap(e, that);
        console.log(e.currentTarget.dataset.key);
        that.wxSearchFn(e.currentTarget.dataset.key);
    },
    wxSearchDeleteKey: function(e) {
        var that = this
        WxSearch.wxSearchDeleteKey(e, that);
    },
    wxSearchDeleteAll: function(e) {
        var that = this;
        WxSearch.wxSearchDeleteAll(that);
    },
    wxSearchTap: function(e) {
        var that = this
        WxSearch.wxSearchHiddenPancel(that);
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})