// testphone.js
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        telephone: '',
        code: '',
        resent: '54秒后重发'
    },

    lastStep: function() {
        wx.navigateBack({

        })
    },

    placeOrder: function() {
        console.log('code=' + this.data.code);
        app.globalData.orderDetail.code = this.data.code;
        console.log(app.globalData.orderDetail);

        if (app.globalData.orderDetail) {
            app.createOrder(app.globalData.orderDetail,
                function(params) {
                    if (params.id && params.id.length == 24) {
                        wx.redirectTo({
                            url: '/pages/placeorder/placesuccess/placesuccess?to_n=' + params.to_n,
                        })
                    }
                },
                function(errMsg) {

                })
        }
    },

    bindGetCode: function(e) {
        this.setData({
            code: e.detail.value
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this;
        app.getSmsCode(function(params) {
                that.setData({
                    telephone: params
                })
            }, function(errMsg) {

            })
            // if (options.phone) {
            //     that.setData({
            //         telephone: options.phone
            //     })
            // }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})