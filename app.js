// wxb35e3aca4403393d
// d171884b246639f20371fbf5d350d4ed
var network = require('/utils/network.min.js');
const CID = '12345678901234567890abcd';

App({
        onLaunch: function() {

        },
        goToLogin: function() {
            if (!this.globalData.isLogin) {
                wx.navigateTo({
                    url: '/pages/login/login',
                })
            }
        },
        fullUrl: function(url) {
            return url + '?cid=' + CID + '&token=' + this.globalData.token
        },
        // 首次访问获取企业ID和token
        getToken: function(success, fail) {
            console.log('token:' + this.globalData.token);
            network.request(this.fullUrl('v1/user/active/status'), 'GET', '',
                function(res) {
                    success(res)
                },
                function(errMsg) {
                    fail(errMsg)
                })
        },
        // 登录
        loginIn: function(params, success, fail) {
            network.request(this.fullUrl('v1/user/login'), 'POST', params,
                function(res) {
                    success(res)
                },
                function(errMsg) {
                    fail(errMsg)
                })
        },
        // 获取话题列表
        getBbsList: function(success, fail) {
            network.request(this.fullUrl('v1/bbs/list/category'), 'GET', '',
                function(res) {
                    success(res)
                },
                function(errMsg) {
                    fail(errMsg)
                })
        },
        // 获取话题列表
        getLicense: function(success, fail) {
            network.request(this.fullUrl('v1/sys/license/demo'), 'GET', '',
                function(res) {
                    success(res)
                },
                function(errMsg) {
                    fail(errMsg)
                })
        },
        // 获取收货信息列表
        getDeliveryList: function(success, fail) {
            this.goToLogin();
            network.request(this.fullUrl('v1/express/delivery/list'), 'GET', '',
                function(res) {
                    success(res)
                },
                function(errMsg) {
                    fail(errMsg)
                })
        },
        // 新增一条收货信息
        addNewDelivery: function(params, success, fail) {
            this.goToLogin();
            network.request(this.fullUrl('v1/express/delivery/add'), 'POST', params,
                function(res) {
                    success(res)
                },
                function(errMsg) {
                    fail(errMsg)
                })
        },
        // 删除一条收货信息
        delDelivery: function(params, success, fail) {
            this.goToLogin();
            network.request(this.fullUrl('v1/express/delivery/remove'), 'GET', params,
                function(res) {
                    success(res)
                },
                function(errMsg) {
                    fail(errMsg)
                })
        },
        // 提交订单
        getSmsCode: function(success, fail) {
            this.goToLogin();
            network.request(this.fullUrl('v1/order/sms'), 'POST', '',
                function(res) {
                    success(res)
                },
                function(errMsg) {
                    fail(errMsg)
                })
        },
        // 提交订单
        createOrder: function(params, success, fail) {
            this.goToLogin();
            network.request(this.fullUrl('v1/order/create'), 'POST', params,
                function(res) {
                    success(res)
                },
                function(errMsg) {
                    fail(errMsg)
                })
        },


        // 错误提示
        warning: function(params) {
            wx.showModal({
                content: params,
                confirmText: '关闭',
                showCancel: false
            })
        },
        // 获取用户信息
        getUserInfo: function(cb) {
            var that = this
            if (that.globalData.userInfo) {
                typeof cb == "function" && cb(that.globalData.userInfo)
            } else {
                //调用登录接口
                wx.login({
                    success: function() {
                        wx.getUserInfo({
                            success: function(res) {
                                that.globalData.userInfo = res.userInfo
                                typeof cb == "function" && cb(that.globalData.userInfo)
                                console.log(res.userInfo)
                            }
                        })
                    }
                })
            }
        },
        globalData: {
            token: '',
            /**
             * 测试服务器
             */
            url_test: 'http://121.201.28.11:6601/',
            /**
             * 产品列表
             */
            products: [],
            product_name: '',
            product_num: 0,
            /**
             * 判断是否登录
             */
            isLogin: false,
            /**
             * 收货信息列表
             */
            addresses: [],
            /**
             * 地址表的下标
             */
            addressIndex: -1,

            deliveryDate: '',

            orderDetail: {},
        }
    })
    // userInfo: null,
    /**
     * 企业ID
     */
    // cid: '',
    /**
     * 通行证
     */
    /**
     * 正式服务器
     */
    // url_official: 'http://121.201.13.231:8080/',

/**
 * 第一次访问应用或退出后重新访问，都需要调用一次该接口
 */
// first_visit: 'v1/user/active/status',
/**
 * 用户登录 
 * POST 
 * params:{mobile:xxx,pwd:xxx}
 */
// login: 'v1/user/login',
/**
 * 话题列表
 */
// bar_list: 'v1/bbs/list/category',
/**
 * 话题详情
 */
// bar_detail: 'v1/bbs/one',